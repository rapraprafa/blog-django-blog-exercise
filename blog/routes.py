from django.urls import path
from . import views

#additional routes for accounts section
urlpatterns = [
    path('', views.index, name='index'),
    path('changeaccdetails', views.changeaccdetails, name='changeaccdetails'),
    path('deletepost<int:num>', views.deletepost, name='deletepost'),
    path('deletecomment<int:num>/<int:postid>', views.deletecomment, name='deletecomment'),
    path('editpost<int:num>', views.editpost, name='editpost'),
    path('post<int:num>', views.post, name='post'),
    path('createpostuser<int:num>', views.createpost, name='createpost'),
    path('comment<int:num>', views.comment, name='comment')
]