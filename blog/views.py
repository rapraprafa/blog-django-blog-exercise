from django.shortcuts import render, redirect
from .models import Post, User, Comment
from datetime import datetime
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    posts = Post.objects.order_by('-datetimeposted')
    return render(request, 'index.html', {'posts': posts})

def post(request, num):
    post = Post.objects.get(pk=num)
    comments = Comment.objects.filter(post_id = num)
    user = request.user
    return render(request, 'postcontent.html', {'post': post, 'comments': comments, 'user': user})

@login_required(login_url='/accounts/signin')
def createpost(request, num):
    user = User.objects.get(pk=num)
    if user.is_authenticated:
        if request.method == 'GET':
                return render(request, 'createpost.html', {'user': user})
        else:
            authorid = user.id
            title = request.POST['title']
            content = request.POST['content']
            datetimeposted = datetime.now
            datetimeupdated = datetime.now
            if user.is_authenticated:
                post = Post(postowner_id=user, title=title, content=content)
                post.save()
                return redirect('/')
            else:
                return render(request, 'signin.html')
    else:
        return redirect('accounts/signin')

@login_required(login_url='/accounts/signin')
def changeaccdetails(request):
    user = request.user
    if request.method == 'GET':
        return render(request, 'changeaccdetails.html', {'user': user})
    else:
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.email = request.POST['email']
        user.save()
        return redirect('/')

@login_required(login_url='/accounts/signin')
def editpost(request, num):
    post = Post.objects.get(pk=num)
    if request.method == 'GET':
        return render(request, 'editpost.html', {'post': post})
    else:
        updatedTitle = request.POST['title']
        updatedContent = request.POST['content']
        post.title = updatedTitle
        post.content = updatedContent
        post.save()
        return redirect('/post'+str(num))

@login_required(login_url='/accounts/signin')
def deletepost(request, num):
    post = Post.objects.get(pk=num)
    post.delete()
    return redirect('/')

@login_required(login_url='/accounts/signin')
def deletecomment(request, num, postid):
    comment = Comment.objects.get(pk=num)
    comment.delete()
    return redirect('/post'+str(postid))

@login_required(login_url='/accounts/signin')
def comment(request, num):
    user = request.user
    content = request.POST['content']
    post = Post.objects.get(pk=num)
    if user.is_authenticated:
        comment = Comment(content=content, commentowner_id=user, post_id=post)
        comment.save()
        link = '/post' + str(num)
        return redirect(link)
    else:
        return render(request, 'signin.html')