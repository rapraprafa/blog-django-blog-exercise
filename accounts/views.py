from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages

# Create your views here.

#sign up route function
def signup(request):
    if request.method == 'POST':
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        email = request.POST['email']
        username = request.POST['username']
        password1 = request.POST['password1']
        password2 = request.POST['password2']


        if password1 == password2:
            if (User.objects.filter(username=username).exists()):
                messages.info(request, 'Username is already used.')
                return redirect('signup')
            elif (User.objects.filter(email=email).exists()):
                messages.info(request, 'Email is already used.')
                return redirect('signup')
            else:
                user = User.objects.create_user(username=username, email=email, password=password1, first_name=firstname, last_name=lastname)
                user.save()
                return render(request, 'signin.html')
        else:
            messages.info(request, 'Passwords do not match.')

    else:
        return render(request, 'signup.html')

#sign in route function
def signin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            print('Signed in successfully')
            return redirect('/')
        else:
            messages.info(request, 'Invalid credentials')
            return redirect('signin')
    else:
        return render(request, 'signin.html')

def signout(request):
    auth.logout(request)
    return redirect('/')

def changepass(request):
    user = request.user
    if request.method == 'GET':
        return render(request, 'changepass.html')
    else:
        currpass = request.POST['currpass']
        newpass = request.POST['newpass']
        if not newpass and user.password != currpass:
            messages.info(request, 'Change password not successful.')
            return redirect('changepass')
        else:
            user.set_password(newpass)
            user.save()
            return redirect('/changeaccdetails')
