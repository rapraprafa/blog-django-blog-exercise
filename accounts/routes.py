from django.urls import path
from . import views

#additional routes for accounts section
urlpatterns = [
    path('changepass', views.changepass, name='changepass'),
    path('signup', views.signup, name='signup'),
    path('signin', views.signin, name='signin'),
    path('signout', views.signout, name='signout')
]