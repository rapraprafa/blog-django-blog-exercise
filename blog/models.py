from django.db import models
from django.contrib.auth.models import User, auth
from datetime import datetime

# Create your models here.

class Post(models.Model):
    postowner_id = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    content = models.TextField()
    datetimeposted = models.DateTimeField(default=datetime.now)
    datetimeupdated = models.DateTimeField(default=datetime.now)

class Comment(models.Model):
    commentowner_id = models.ForeignKey(User, on_delete=models.CASCADE)
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE)
    content = models.TextField()
    datetimecommented = models.DateTimeField(default=datetime.now)
    datetimeupdated = models.DateTimeField(default=datetime.now)